package com.cms.kernel.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.cms.kernel.util.DateTimeUtil;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:UserDetail.java
 * 创建:Well
 * 日期:2016年6月2日 上午8:36:18
 * 来自:
 * 版本:0.9.1
 * 描述:用户的详细信息类，UserDetail类与User类是一对一的关系
 * 		定义用户的其它信息
 */

@Entity
@Table(name="t_user_detail")
public class UserDetail extends ABaseEntity
{
	@Column(name="user_id")
	private String userID; // 与用户表(t_user)的id主键关联
	
	@Column(name="nickname")
	private String nickname; // 用户昵称
	
	@Column(name="gender")
	private Integer gender; // 性别，1---男，2---女 
	
	@JsonFormat(pattern = DateTimeUtil.DATE_FORMAT, timezone = DateTimeUtil.TIME_ZONE)
	@Column(name = "birthday")
	private Date birthday; // 用户生日，通过使用 @JsonFormat 自定义在 @ResponseBody 注解中返回的json中的 java.util.Date类型的转换格式
	
	@Column(name="height")
	private String height; // 身高
	
	@Column(name="weight")
	private String weight; // 体重

	// TODO:其它属性
	
	public String getUserID()
	{
		return userID;
	}

	public void setUserID(String userID)
	{
		this.userID = userID;
	}

	public String getNickname()
	{
		return nickname;
	}

	public void setNickname(String nickname)
	{
		this.nickname = nickname;
	}

	public Integer getGender()
	{
		return gender;
	}

	public void setGender(Integer gender)
	{
		this.gender = gender;
	}

	public Date getBirthday()
	{
		return birthday;
	}

	public void setBirthday(Date birthday)
	{
		this.birthday = birthday;
	}

	public String getHeight()
	{
		return height;
	}

	public void setHeight(String height)
	{
		this.height = height;
	}

	public String getWeight()
	{
		return weight;
	}

	public void setWeight(String weight)
	{
		this.weight = weight;
	}

}
