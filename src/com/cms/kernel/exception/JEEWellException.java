package com.cms.kernel.exception;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:JEEWellException.java
 * 创建:Well
 * 日期:2016年6月28日 上午9:13:15
 * 来自:
 * 版本:
 * 描述:自定义jeewell的异常，继承RuntimeException
 */

public class JEEWellException extends RuntimeException
{
	/**
	 * 序列化版本ID
	 */
	private static final long serialVersionUID = 1L;

	public JEEWellException()
	{
		super("系统运行出现异常");
	}
	
	public JEEWellException(String message)
	{
		super(message);
	}
	
}
