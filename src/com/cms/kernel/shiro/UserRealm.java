package com.cms.kernel.shiro;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:UserRealm.java
 * 创建:Well
 * 日期:2016年6月23日 下午3:20:39
 * 来自:
 * 版本:
 * 描述:前台用户的权限校验
 */

public class UserRealm extends AuthorizingRealm
{

	/**
	 * 用户授权和用户赋予角色，只是在缓存中没有用户的授权信息时调用
     * 负责在应用程序中决定用户的访问控制的方法
     * @param principalCollection
     * @return 
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection)
	{
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 登录校验
     * 当调用subject.login方法时，进入该方法
     * @param authenticationToken
     * @return
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException
	{
		// TODO Auto-generated method stub
		return null;
	}
	
}
