<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/admin/taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>后台登录</title>
	<script type="text/javascript" src="${ctxAsset}/common/js/jquery-1.12.4.min.js"></script>
</head>
<body>

	<form>
		<input type="text" id="username" name="username" value="admin" />
		<input type="password" id="password" name="password" value="admin" />
		<br/>
		<input type="text" id="captcha" name="captcha" />
		<img id="captcha_img" src="${ctx}/captcha/imageCaptcha.jpg" alt="正在加载验证码" onclick="this.src='${ctx}/captcha/imageCaptcha.jpg?'+new Date()" title="单击更换验证码" style="cursor: pointer;" />
		<br/>
		<input type="button" id="login" value="登录" />
	</form>

</body>

<script type="text/javascript">

	$(function(){
		
		$("#login").click(function(){
			var username=$("#username").val();
			var password=$("#password").val();
			var captcha=$("#captcha").val();
			
			$.post("${ctx}/admin/login",{username:username,password:password,captcha:captcha},function(data){
				console.log("data："+data);
			});
		});
	});

</script>

</html>